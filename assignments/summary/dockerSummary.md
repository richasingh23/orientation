# **dockerBasics**   

### **Why docker ?**

Docker is used to deploy our projects effectively. As it is difficult to manage and deploy projects in different environments so it enables us to separate our applications from your infrastructure so we can deliver software quickly..      
![](https://iotbytes.files.wordpress.com/2017/06/iot_containers.png?w=541&h=356)  

## **Docker Terminologies**  
**1.Docker**  
* Program for developers to develop and run applications with containers.

**2.Docker Images**
* contains code, runtime, libraries, environment variables and configuration files.

**3.Container**
* Running docker images
* from one image we can create multiple containers.

**4.Docker Hub**
* It is like a github for docker images and containers.

### **Docker Installation**

1.Installing CE (Community Docker Engine)  
     
      $ sudo apt-get update
      $ sudo apt-get install \
          apt-transport-https \
          ca-certificates \
          curl \
          gnupg-agent \
          software-properties-common
      $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
      $ sudo apt-key fingerprint 0EBFCD88
      $ sudo add-apt-repository \
         "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
      $(lsb_release -cs) \
          stable nightly test"
      $ sudo apt-get update
      $ sudo apt-get install docker-ce docker-ce-cli containerd.io

       // Check if docker is successfully installed in your system
      $ sudo docker run hello-world

## **Docker Basic Commands**

**$ docker ps**  allows us to view all the containers that are running on the Docker Host.

**$ docker start**  starts any stopped container(s).

**$ docker stop**  stops any running container(s).

**$ docker run**  it creates containers from docker images.

**$ docker rm**    it deletes the containers.

**$ docker cp**   copy file inside docker.

**$ docker exec**   give permission to run shell script.

**$ docker tag**  tag image name with different name.

**$ docker push**   push on dockerhub.

## **Common Operations On Dockers**
     > Download/pull the docker images that you want to work with.
     > Copy your code inside the docker
     > Access docker terminal
     > Install and additional required dependencies
     > Compile and Run the Code inside docker
     > Document steps to run your program in README.md file
     > Commit the changes done to the docker.
     >Push docker image to the docker-hub and share repository with people who want to try your code.        
     
![](https://dzone.com/storage/temp/5288806-docker-stages.png?w=541&h=356)
