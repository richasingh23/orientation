#### **GIT**

### **What is git ?**  

Git is a _version-control_ software that keeps track of every revision you and your team makes during the development of a software.
          ![](https://www.xpertup.com/wp-content/uploads/2019/04/git.jpg?w=441&h=246)

### **Required Vocabulary**

 * **Repository:**
    It is a collection of all code files and folders.It's the big box where team members through their codes. 

 * **GitLab:**
    The second most popular remote storage solution for git repos.

 * **Commit:**
    It is same as saving your work.It only exists on your local machine until it is pushed to a remote repository.

 * **Push:**
    It is essentially syncing your commits to gitlab.

 * **Branch:**  
    Git repos are similar to a tree. 
    Master Branch
    * Branch 1
    * Branch 2....

 * **Merge:**
    Integrating two branches together.

 * **Clone:** 
    It takes the entire online repository and creates an exact copy of it in your machine.

 * **Fork:**
    Same as cloning, the only difference is that here you get an entirely new repo of that code in your own name.       
     
    ![](https://miro.medium.com/max/1106/1*HJIbiE3SWPtAmJP1wgvOTg.png?w=441&h=246)      

### **Getting started and required command**

**How to install git**
* For _Linux_:

            sudo apt-get install git 

* For _Windows_:
    download the installer and run it.

### **Git Internals**   
* Modified    
* Staging     
* Commit        
![Git stages](https://lh3.googleusercontent.com/proxy/P6WSZ9WqEeWLOwWlBDtxRcDw63yWVtFuKkd2_s35CxDFR6gPZzuQlorwopkVjncAADw-XZA0WvY3dSVsxUjCZ5Jvxt0KewVbWeWr2Z5TP0o)                

### **Git Workflow**  
![Git Workflow](https://docs.onosproject.org/images/contributing_workflow.png?w=441&h=246)
















       